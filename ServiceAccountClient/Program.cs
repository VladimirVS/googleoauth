﻿using System;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using System.Text;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth;

namespace ServiceAccountClient
{
    class Program
    {
        static string GetData(string address)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(address);
            // Set some reasonable limits on resources used by this request
            webRequest.MaximumAutomaticRedirections = 4;
            webRequest.MaximumResponseHeadersLength = 4;
            // Set credentials to use for this request.
            string ACC_MAIL = "cloudhqsa@integrations-182814.iam.gserviceaccount.com";
            var serviceAccountCredentialInitializer = new ServiceAccountCredential.Initializer(SERVICE_ACCT_EMAIL)
            {
                User = "oss@vuwall.com", // A user with administrator access.
                Scopes = new[] { "https://apps-apis.google.com/a/feeds/emailsettings/2.0/" }
            }.FromCertificate(certificate);
            webRequest.Credentials = wGoogleCredential.FromServiceAccountCredential(new ServiceAccountCredential(serviceAccountCredentialInitializer));
            webRequest.Credentials = cred;
            HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
            Console.WriteLine("Content length is {0}", response.ContentLength);
            Console.WriteLine("Content type is {0}", response.ContentType);

            // Get the stream associated with the response.
            Stream receiveStream = response.GetResponseStream();

            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            Console.WriteLine("Response stream received.");
            var answer = readStream.ReadToEnd();
            Console.WriteLine(answer);
            response.Close();
            readStream.Close();
            return answer;
            //string result = null;
            //HttpClient Client = new HttpClient();
            //HttpResponseMessage MSG = await Client.GetAsync(address);
            //if (MSG.IsSuccessStatusCode)
            //{
            //    result = await MSG.Content.ReadAsStringAsync();
            //}
            //return result;
        }
        static void Main(string[] args)
        {
            var address = "https://integration.teamworkinsight.com/test07chq/rest/SvcInfo/GetList";
            var values = GetData(address);
            if(values == null)
            {
                Console.WriteLine("Bad Request");
            }
            else
            {
                Console.WriteLine(values);
            }
        }
    }
}
